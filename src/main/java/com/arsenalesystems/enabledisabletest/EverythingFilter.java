package com.arsenalesystems.enabledisabletest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import java.io.IOException;

public class EverythingFilter implements Filter
{
    private static final Logger log = LoggerFactory.getLogger(EverythingFilter.class);
    private TestBean testBean;

    public EverythingFilter(TestBean bustMyCache)
    {
        this.testBean = bustMyCache;
    }

    public void init(FilterConfig filterConfig)
    {
        testBean.calledFromServletFilter();
    }

    public void destroy()
    {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        chain.doFilter(request, response);
    }
}