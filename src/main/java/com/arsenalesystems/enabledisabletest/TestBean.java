package com.arsenalesystems.enabledisabletest;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class TestBean implements InitializingBean, DisposableBean
{
    private static final Logger log = LoggerFactory.getLogger(TestBean.class);
    private EventPublisher eventPublisher;
    private PluginController pluginController;

    public TestBean(EventPublisher eventPublisher,
                    PluginController pluginController)
    {
        log.debug("TestBean constructor");

        this.eventPublisher = eventPublisher;
        this.pluginController = pluginController;

        eventPublisher.register(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        log.debug("Tester Thread in afterPropertiesSet");
    }


    public void calledFromServletFilter()
    {
        log.debug("Begin calledFromServletFilter");

        for (int i=0; i < 3; i++)
        {

            long start = System.currentTimeMillis();
            log.debug("Begin disabling plugin module");
            pluginController.disablePluginModule("confluence.sections.admin:dailybackup");
            log.debug("Done disabling plugin module: elapsed time = {}", System.currentTimeMillis() - start);


            start = System.currentTimeMillis();
            log.debug("Begin enabling plugin module");
            pluginController.enablePluginModule("confluence.sections.admin:dailybackup");
            log.debug("Done enabling plugin module: elapsed time = {}", System.currentTimeMillis() - start);
        }

        log.debug("End calledFromServletFilter");
    }

    @Override
    public void destroy() throws Exception
    {
        log.debug("TestBean destroy");

        eventPublisher.unregister(this);
    }

    @EventListener
    public void gotEvent(PluginEnabledEvent ev)
    {
        log.info("Got PluginEnabledEvent for: {}", ev.getPlugin().getKey());
    }

    @EventListener
    public void gotEvent(PluginDisabledEvent ev)
    {
        log.info("Got PluginDisabledEvent for: {}", ev.getPlugin().getKey());
    }

    @EventListener
    public void gotEvent(PluginModuleEnabledEvent ev)
    {
        log.info("Got PluginModuleEnabledEvent for: {}", ev.getModule().getKey());
    }

    @EventListener
    public void gotEvent(PluginModuleDisabledEvent ev)
    {
        log.info("Got PluginModuleDisabledEvent for: {}", ev.getModule().getKey());
    }
}
